// Nathan Stowwe
// Arma 3 Lightweight Arsenals
// Showcase Controller pylon
// 6-April-2020

params ["_obj"];

_obj addAction [
	"<t color='#AAAAFF'>Add 'soldier' unit trait</t>",
	{
		params ["_nil", "_caller"];
		_caller setUnitTrait ["soldier", true, true];
		hint "Trait added!";
		sleep 3;
		hint "";
	}
];
_obj addAction [
	"<t color='#AAAAFF'>Remove 'soldier' unit trait</t>",
	{
		params ["_nil", "_caller"];
		_caller setUnitTrait ["soldier", false, true];
		hint "Trait removed!";
		sleep 3;
		hint "";
	}
];
_obj addAction [
	"<t color='#AAAAFF'>Add 'civ' unit trait</t>",
	{
		params ["_nil", "_caller"];
		_caller setUnitTrait ["civ", true, true];
		hint "Trait added!";
		sleep 3;
		hint "";
	}
];
_obj addAction [
	"<t color='#AAAAFF'>Remove 'civ' unit trait</t>",
	{
		params ["_nil", "_caller"];
		_caller setUnitTrait ["civ", false, true];
		hint "Trait removed!";
		sleep 3;
		hint "";
	}
];
