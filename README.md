# Arma 3 - Stowwe Arsenals

## This is a small script meant to create whitelistable arsenals.

### WARNING: REQUIRES ACE! 

### Features:
 - Create your own arsenal whitelists, and decide who can access what gear.
 - Use common lists to simplify your work. Give everyone basic equipment, but each group their own toys...
 - Lightweight, only a couple KB
 - No group limits, add as many groups you need!
 - Self contained, add as many arsenals as you need!

### Installing:
Included in this repository is a sample mission on Stratis. Looking in the init should give you a rough idea of how to initialize your own arsenal. This currently CANNOT be done through the object's init field. I'm not sure why. That's on the list of things to fix.

First thing you'll need to do its initialize the script. Do that by adding this to your init.sqf

    call compile preprocessFile "stowwe_arsenals\init.sqf";

You should delete everything besides the stowwe_arsenals directory, none of the scripts require it.

### Initializing a box:

Basic syntax for initialization is as follows:

    
    [
    OBJECT                          // The object to make into an arsenal.
    ARRAY                           // The whitelist of groups and lists. More information below.
    ARRAY                           // The position of the physical arsenal boxes on the map. These should be inaccessible. If unsure, set to [0, 0, 0]
    STRING                          // The classname for the physical box. If unsure, use "Land_WoddenBox_F"
    ] call stowwe_ars_initBox;
    
As for the whitelists, they're a tad more complicated, but not too much so.

    _nav = ["ItemMap", "ItemGPS", "ItemCompass"];
    _MX = ["arifle_MX_F", "arifle_MX_GL_F"];
    
    // Anyone with the unit trait "civ" will only have access to navigation equipment...
    // But anyone with the soldier trait will have nav equipment and MXes.
    
    _whitelist = [
        ["civ", [_nav]],
        ["soldier", [_nav, _MX]]
    ];

The arguments for each group is `["unittrait", [itemarr1, itemarr2, itemarr3...]]`. To set unit traits, put the following in a unit's init field:

    this setUnitTrait ["yourTraitName", true, true];

Configurations can be changed in the stowwe_arsenals\config.sqf file. As of writing its pretty barebones, more customization options will come in the future.

Also note that you can add several unit traits to one unit. Due to this, this script only loads the FIRST trait that it comes across in its list of traits. This list is in the same whitelist order that the arsenal was initialized with, so if you initialized group1, group2, and group3, and then someone with group3 and group2 opens it, they'll open the group2 arsenal.

### Cloning a box:

To clone box, simply call the `stowwe_ars_cloneBox` method with the source and destination box.

    [oldBox, someNewBox] call stowwe_ars_cloneBox;

This should go without saying, but you should synchronosuly call this, and make sure its called AFTER the previous box is initialized.