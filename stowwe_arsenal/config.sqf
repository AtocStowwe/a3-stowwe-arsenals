// Nathan Stowwe
// Arma 3 Lightweight Arsenals
// Configuration file
// 16-April-2020

stowwe_ars_var_interactionRange = { 10 };		// Distance (in meters) from the origin of the object to show interactions
stowwe_ars_var_interactionColor = {"#6666FF"};	// Color of the arsenal's name
stowwe_ars_var_sleepTime = { 1 };				// Time in seconds for the script to sleep between when the player selects "Open Arsenal" and the arsenal actually opening.
stowwe_ars_var_prompt = {"--Open ACE Virtual Arsenal--"}	// The prompt to give the player for the arsenal.
