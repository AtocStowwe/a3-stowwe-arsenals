// Nathan Stowwe
// Arma 3 Lightweight Arsenals
// Functions file
// 16-April-2020

stowwe_ars_initBox = {
	params ["_arsenalbox", "_par", "_loc", "_prop"];
	if(isServer) then {
		_lists = [];

		{
			_master = _x;
			// get the group name and list of arsenal arrays.
			_group = + _master select 0;
			_arsenals = + _master select 1;

			//for each element in each array,
			_whitelist = [];
			{
				_temp = _x;
				{
					//push it to the back of _whitelist only if its unique
					_whitelist pushBackUnique _x;
					//diag_log _x;
				} forEach (_temp);
			} forEach (_arsenals);

			// ars_foobar for group "foobar";
			_nam = ["ars_", _group] joinString "";

			// create vehicle, disable simulation, and set the name to ars_xyz
			_box = createVehicle [_prop, _loc];
			_box setPosATL _loc;
			_box enableSimulation false;
			_box setVehicleVarName _nam;

			[_box, false] call ace_dragging_fnc_setDraggable;
			[_box, false] call ace_dragging_fnc_setCarryable;

			// initialize the ace arsenal
			[_box, _whitelist] call ace_arsenal_fnc_initBox;

			// and finally append it to the list of local arsenals
			_lists append [[_group, _box]];

		} forEach (_par);

		// save the list.
		_arsenalbox setvariable ["stowwe_ars_localArsenals", _lists, true];
	};
	[_arsenalbox] call stowwe_ars_addInteraction;
};

stowwe_ars_addInteraction = {
	params ["_box"];

	_int = ["<t color='", call stowwe_ars_var_interactionColor, "'>", call stowwe_ars_var_prompt, "</t>"] joinString "";
	_box addAction [
		_int,
		stowwe_ars_openBox,
		[],
		100,
		false,		// show in center of window, not needed.
		true,		// hide on use, vanity.
		"",			// key to actiavte, not needed
		"true",		// _target, _this
		call stowwe_ars_var_interactionRange	// radius in which it'll show up.
	];
};

stowwe_ars_openBox = {
	params ["_target", "_caller"];

	_list = _target getVariable "stowwe_ars_localArsenals";
	_access = false;
	{
		if (_caller getUnitTrait (_x select 0)) then {
			_out = ["Opening ", _x select 0, " arsenal..."] joinString "";
			hint _out;
			sleep call stowwe_ars_var_sleepTime;
			hint "";
			[call {_x select 1}, _caller] call ace_arsenal_fnc_openBox;
			_access = true;
		};
	} forEach (_list);
	if(!_access) then {
		hint "You don't have access to this arsenal.";
	};
};

stowwe_ars_cloneBox = {
	params ["_oldbox", "_newbox"];

	_newbox setVariable ["stowwe_ars_localArsenals", (_oldbox getVariable "stowwe_ars_localArsenals")];
	[_newbox] call stowwe_ars_addInteraction;
}
