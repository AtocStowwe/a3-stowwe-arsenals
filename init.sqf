// Nathan Stowwe
// Arma 3 Lightweight Arsenals
// Mission initialization file
// 16-April-2020

call compile preprocessFile "stowwe_arsenal\init.sqf";

_nav = ["ItemMap", "ItemGPS", "ItemCompass"];
_MX = ["arifle_MX_F", "arifle_MX_GL_F"];

// Anyone with the unit trait "civ" will only have access to navigation equipment...
// But anyone with the soldier trait will have nav equipment and MXes.

_whitelist = [
  ["civ", [_nav]],
  ["soldier", [_nav, _MX]]
];


[arsenalBox, _whitelist, [1804, 5583, 0], "Land_WoodenBox_F"] call stowwe_ars_initBox;
[arsenalBox, newBox] call stowwe_ars_cloneBox;
